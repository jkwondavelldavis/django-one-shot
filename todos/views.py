from django.shortcuts import render
from todos.models import TodoList, TodoItem
from todos.form import CreateForm, EditForm, ItemForm
from django.shortcuts import redirect


def todo_list_list(request):
    todolist = TodoList.objects.all()

    context = {
        "todolist": todolist,
    }
    return render(request, "todos/home.html", context)


def todo_list_detail(request, id):
    listdetail = TodoList.objects.get(id=id)
    context = {
        "listdetail": listdetail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            create = form.save()
            return redirect("todo_list_detail", id=create.id)

    else:
        form = CreateForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    update = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = EditForm(request.POST, instance=update)
        if form.is_valid():
            update = form.save()
            return redirect("todo_list_detail", id=update.id)

    else:
        form = EditForm(instance=update)
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete.delete()
        return redirect("home")

    else:
        context = {
            "delete": delete,
        }
    return render(request, "todos/delete.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.id)

    else:
        form = ItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/item.html", context)


def todo_item_update(request, id):
    update = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=update)
        if form.is_valid():
            update = form.save()
            return redirect("todo_list_detail", id=update.id)

    else:
        form = ItemForm(instance=update)
    context = {
        "form": form,
    }
    return render(request, "todos/itemedit.html", context)
